
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>WorkLab - <decorator:title /></title>
<%@ page session="false"%>

    <link rel="stylesheet"
    	href="<c:url value='/assets/css/foundation.css' />">
    <link rel="stylesheet" href="<c:url value='/assets/css/worklab.css' />">
    <link rel="stylesheet"
    	href="<c:url value='/assets/font-awesome/css/font-awesome.min.css' />">
    <script src="<c:url value='/assets/js/vendor/jquery.js' />"></script>
    <script src="<c:url value='/assets/js/vendor/custom.modernizr.js' />"></script>
    <script src="<c:url value='/assets/js/worklab.js' />"></script>
<title></title>
<decorator:head />

</head>
<body>
	<nav class="top-bar hide-for-small">
		<ul class="title-area">
			<!-- Title Area -->
			<li class="name">
				<h1>
					<a href="<c:url value='/' />">WorkLab</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>
		<section class="top-bar-section">
			<!-- Right Nav Section -->
			<ul class="right">
				<li class="divider"></li>
				<li class=""><a href="#">Help</a></li>
				<li class="divider"></li>
				<!-- <li class="divider"></li>
				<li class=""><a class="button alert" href="#">Register</a></li>
				<li class="divider"></li>
				<li class="has-form"><a class="button success" href="#">Login</a></li>
				 -->
			</ul>
		</section>
	</nav>

	<decorator:body />
</body>
</html>