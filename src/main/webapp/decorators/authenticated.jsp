<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
           prefix="decorator" %>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>WorkLab - <decorator:title/></title>
    <%@ page session="true" %>

    <link rel="stylesheet"
          href="<c:url value='/assets/css/foundation.css' />">
    <link rel="stylesheet" href="<c:url value='/assets/css/worklab.css' />">
    <link rel="stylesheet"
          href="<c:url value='/assets/font-awesome/css/font-awesome.min.css' />">
    <script src="<c:url value='/assets/js/vendor/jquery.js' />"></script>
    <script src="<c:url value='/assets/js/vendor/custom.modernizr.js' />"></script>
    <script src="<c:url value='/assets/js/foundation/foundation.js' />"></script>
    <script src="<c:url value='/assets/js/foundation/foundation.alerts.js' />"></script>
    <script src="<c:url value='/assets/js/foundation/foundation.dropdown.js' />"></script>
    <script src="<c:url value='/assets/js/foundation/foundation.section.js' />"></script>
    <script src="<c:url value='/assets/js/worklab.js' />"></script>
    <title></title>
    <decorator:head/>

</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="header-content">
            <nav class="top-bar hide-for-small">
                <ul class="title-area">
                    <!-- Title Area -->
                    <li class="name">
                        <h1>
                            <a href="<c:url value='/' />">WorkLab</a>
                        </h1>
                    </li>
                    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>
                <section class="top-bar-section">
                    <!-- Right Nav Section -->
                    <ul class="right">
                        <li class="divider"></li>
                        <li class=""><a
                                href="<c:url value='/authenticated/myprofile'/>">Welcome ${sessionScope.sessionUsername}</a>
                        </li>
                        <li class="divider"></li>
                        <li class="divider"></li>
                        <li class="has-form"><a class="button alert"
                                                href="<c:url value='/logout'/>">Logout</a></li>
                        <!-- <li class="divider"></li>
            <li class=""><a class="button alert" href="#">Register</a></li>
            <li class="divider"></li>
            <li class="has-form"><a class="button success" href="#">Login</a></li>
             -->
                    </ul>
                </section>
            </nav>
        </div>
    </div>
    <div id="content">
        <div id="sidebar">
            <section class="main">
                <div class="large-12 columns end">
                    <ul class="side-nav">
                        <li class="active"><a href="<c:url value='/authenticated' />"><i
                                class="icon-home icon-5x"></i><br/>My WorkLab</a></li>
                        <li class="divider"></li>
                        <li class="active"><a href="<c:url value='/authenticated/mygroups' />"><i
                                class="icon-group icon-5x"></i><br/>My Groups</a></li>
                        <li class="divider"></li>
                        <li class="active"><a href="<c:url value='/authenticated/myprofile' />"><i
                                class="icon-user icon-5x"></i><br/>My
                            Profile</a></li>
                        <li class="divider"></li>
                        <li class="active"><a href="<c:url value='/logout' />"><i class="icon-signout icon-5x"></i><br/>Log
                            out</a></li>
                    </ul>
                </div>
            </section>
        </div>
        <div id="main">
            <section class="main">
                <div class="row">
                    <div class="large-12">
                        <decorator:body/>
                    </div>

                </div>
            </section>
        </div>
        <div id="sidebar-right">
            <div class="large-12 columns">
                <form class="searchform">
                    <input type="text"
                           onblur="if (this.value == '') {this.value = 'Search...';}"
                           onfocus="if (this.value == 'Search...') {this.value = '';}"
                           value="Search..." class="searchfield"> <input
                        type="button" value="Go" class="searchbutton">
                </form>
                <h5>Recent Activity</h5>
                List recent 'global' activities here.
            </div>
        </div>
    </div>
    <div class="push"></div>
</div>
<div id="footer"></div>
<script>
  document.write('<script src=/assets/js/vendor/'
    + ('__proto__' in {} ? 'zepto' : 'jquery')
    + '.js><\/script>');
</script>
<script>
  $(document).foundation();
</script>
</body>
</html>