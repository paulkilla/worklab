<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <%@ page session="true" %>
    <%@ page isELIgnored="false" %>
    <title>${group.cn}</title>
</head>

<body>
<ul class="breadcrumbs">
    <li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
    <li><a href="<c:url value='/authenticated/mygroups'/>">Group</a></li>
    <li><a href="<c:url value='/authenticated/group/${group.cn}'/>">${group.groupName}</a></li>
    <li class="current"><a href="#">Pages</a></li>
</ul>
<h2>Group - ${group.groupName}</h2>
<dl class="sub-nav">
    <dd><a href="<c:url value='/authenticated/group/${group.cn}'/>">News Feed</a></dd>
    <dd>|</dd>
    <dd class="active"><a href="<c:url value='/authenticated/group/${group.cn}/pages'/>">Pages</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/posts'/>">Posts</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/attachments'/>">Attachments</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/events'/>">Events</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/tasks'/>">Tasks</a></dd>
</dl>
<c:if test="${not empty messages}">
    <div class="alert-box success">${messages}</div>
</c:if>
<div class="row">
    <a href="/authenticated/group/worklab/pages/create">Create Page</a>

    <div class="large-12">
        <h3>Pages</h3>

        <div class="large-6 columns">
            <div class="panel">
                <h5>Recent Pages</h5>
                <c:forEach items="${pages}" var="page">
                    <section class="section" style="border-bottom-style:solid;margin-bottom:20px;border-bottom-width:1px;">
                        <p class="title"><a href="#">${page.creator}</a> <c:choose><c:when
                                test="${page.revision_id == 1}">created</c:when><c:otherwise>updated</c:otherwise></c:choose>
                            the page <a
                                    href="<c:url value='/authenticated/group/${group.cn}/page/${page.id.pageId}/show'/>">${page.title}</a> ${page.prettyTime}
                        </p>
                    </section>
                </c:forEach>
            </div>
        </div>
        <div class="large-6 columns">
            <div class="panel">
                <h5>My Pages</h5>
                <table>
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th width="100%">Title</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${pages}" var="page">
                        <c:if test="${page.creator == sessionScope.sessionUsername}">
                            <tr>
                                <td>${page.date_created}</td>
                                <td>
                                    <a href="<c:url value='/authenticated/group/${group.cn}/page/${page.id.pageId}'/>">${page.title}</a>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

</body>
</html>