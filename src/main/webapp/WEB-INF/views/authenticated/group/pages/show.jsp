<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <%@ page session="false" %>
    <%@ page isELIgnored="false" %>
    <title>${group.cn}</title>
</head>

<body>
<ul class="breadcrumbs">
    <li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
    <li><a href="<c:url value='/authenticated/mygroups'/>">Group</a></li>
    <li><a href="<c:url value='/authenticated/group/${group.cn}'/>">${group.groupName}</a></li>
    <li><a href="<c:url value='/authenticated/group/${group.cn}/pages'/>">Pages</a></li>
    <li class="current"><a href="#">${page.title}</a></li>
</ul>
<h2>Group - ${group.groupName}</h2>
<dl class="sub-nav">
    <dd><a href="<c:url value='/authenticated/group/${group.cn}'/>">News Feed</a></dd>
    <dd>|</dd>
    <dd class="active"><a href="<c:url value='/authenticated/group/${group.cn}/pages'/>">Pages</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/posts'/>">Posts</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/attachments'/>">Attachments</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/events'/>">Events</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/tasks'/>">Tasks</a></dd>
</dl>

<div class="panel">
    <h5>${page.title}</h5>

    <div class="row">
        <div class="small-12">
            <div class="row">
                <div class="small-12 columns left">
                    <c:out value="${page.content.htmlContent}" escapeXml="false"/>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>