<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <%@ page session="false" %>
    <%@ page isELIgnored="false" %>
    <title>${group.cn}</title>
</head>

<body>
<ul class="breadcrumbs">
    <li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
    <li><a href="<c:url value='/authenticated/mygroups'/>">Group</a></li>
    <li><a href="<c:url value='/authenticated/group/${group.cn}'/>">${group.groupName}</a></li>
    <li><a href="<c:url value='/authenticated/group/${group.cn}/pages'/>">Pages</a></li>
    <li class="current"><a href="#">Create...</a></li>
</ul>
<h2>Group - ${group.groupName}</h2>
<dl class="sub-nav">
    <dd><a href="<c:url value='/authenticated/group/${group.cn}'/>">News Feed</a></dd>
    <dd>|</dd>
    <dd class="active"><a href="<c:url value='/authenticated/group/${group.cn}/pages'/>">Pages</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/posts'/>">Posts</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/attachments'/>">Attachments</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/events'/>">Events</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/group/${group.cn}/tasks'/>">Tasks</a></dd>
</dl>

<div class="panel">
    <h5>Create Page</h5>
    <c:if test="${not empty messages}">
        <div class="alert-box success">${messages}</div>
    </c:if>
    <c:url var="actionUrl" value="/authenticated/group/${group.cn}/pages/create"/>
    <form:form action="${actionUrl}" autocomplete="off" id="createpage"
               commandName="createpage" method="POST">
        <form:errors path="*" cssClass="alert-box alert" element="div"/>
        <div class="row">
            <div class="small-6">
                <div class="row">
                    <div class="small-10 columns left">
                        <form:label path="title" cssClass="right inline">Page Title: </form:label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-10 columns left">
                        <form:input path="title"/>
                    </div>
                </div>
            </div>
        </div>
        <br/>

        <div class="row">
            <div class="small-6">
                <div class="row">
                    <div class="small-10 columns left">
                        <form:textarea rows="40" path="content.htmlContent"/>
                    </div>
                </div>
            </div>
        </div>
        <br/>

        <div class="row">
            <div class="small-5">
                <div class="row">
                    <div class="small-10 columns right">
                        <input type="submit" class="right button success"
                               value="Create Page"/>
                    </div>
                </div>
            </div>
        </div>
    </form:form>
</div>

</body>
</html>