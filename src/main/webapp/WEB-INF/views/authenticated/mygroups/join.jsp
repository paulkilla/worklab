<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <%@ page session="false" %>
    <%@ page isELIgnored="false" %>
    <title>My Groups</title>
</head>

<body>
<ul class="breadcrumbs">
    <li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
    <li><a href="<c:url value='/authenticated/mygroups'/>">My Groups</a></li>
    <li class="current"><a href="#">Join</a></li>
</ul>
<h2>My Groups</h2>
<dl class="sub-nav">
    <dd><a href="<c:url value='/authenticated/mygroups'/>">Overview</a></dd>
    <dd>|</dd>
    <dd class="active"><a href="<c:url value='/authenticated/mygroups/join'/>">Join</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/leave'/>">Leave</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/administer'/>">Administer</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/create'/>">Create...</a></dd>
</dl>

<div class="panel">

    <c:choose>
        <c:when test="${not empty groups}">
            <h6>You can join to the following public groups:</h6>
            <table>
                <thead>
                <tr>
                    <th width="200">Key</th>
                    <th width="200">Name</th>
                    <th>Description</th>
                    <th>Apply</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${groups}" var="group">
                    <tr>
                        <td>${group.cn}</td>
                        <td>${group.groupName}</td>
                        <td>${group.description}</td>
                        <td><a href="<c:url value='/authenticated/mygroups/${group.cn}/join'/>"
                               class="button success applyGroup" style="margin-bottom:0px">Join</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:when>
        <c:otherwise>
            <h6>There are no public groups to join.</h6>
        </c:otherwise>
    </c:choose>

</div>

</body>
</html>