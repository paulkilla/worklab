<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <%@ page session="false" %>
    <%@ page isELIgnored="false" %>
    <title>My Groups</title>
</head>

<body>
<ul class="breadcrumbs">
    <li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
    <li><a href="<c:url value='/authenticated/mygroups'/>">My Groups</a></li>
    <li class="current"><a href="#">Administer</a></li>
</ul>
<h2>My Groups</h2>
<dl class="sub-nav">
    <dd><a href="<c:url value='/authenticated/mygroups'/>">Overview</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/join'/>">Join</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/leave'/>">Leave</a></dd>
    <dd>|</dd>
    <dd class="active"><a href="<c:url value='/authenticated/mygroups/administer'/>">Administer</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/create'/>">Create...</a></dd>
</dl>

<div class="panel">
<c:if test="${not empty messages}">
    <div class="alert-box success">${messages}</div>
</c:if>
<div class="row">
    <div class="small-6 large-6 columns"><h5>Administering ${group.groupName}</h5></div>
    <div class="small-6 large-6 columns"><h5>Members</h5></div>
</div>
<div class="row">
<div class="small-6 large-5 columns">
    <c:url var="actionUrl" value="/authenticated/mygroups/administer/${groupCn}"/>
    <form:form action="${actionUrl}" autocomplete="off" id="group"
               commandName="group" method="POST">
        <form:errors path="*" cssClass="alert-box alert" element="div"/>

        <div class="row">
            <div class="small-3 columns">
                <form:label path="cn" cssClass="right inline">Group Key: </form:label>
            </div>
            <div class="small-9 columns left">
                <form:label path="cn" cssClass="inline">${groupCn}</form:label>
            </div>
        </div>

        <br/>


        <div class="row">
            <div class="small-3 columns">
                <form:label path="groupName" cssClass="right inline">Group Name: </form:label>
            </div>
            <div class="small-9 columns left">
                <form:input path="groupName"/>
            </div>
        </div>

        <br/>


        <div class="row">
            <div class="small-3 columns">
                <form:label path="description" cssClass="right inline">Description: </form:label>
            </div>
            <div class="small-9 columns left">
                <form:textarea path="description"/>
            </div>
        </div>

        <br/>


        <div class="row">
            <div class="small-3 columns">
                <form:label path="publicGroup" cssClass="right inline">Public Group? </form:label>
            </div>
            <div class="small-9 columns left">
                <form:checkbox path="publicGroup"/>
            </div>
        </div>


        <div class="row">
            <div class="small-10 columns right">
                <input type="submit" class="right button success small"
                       value="Update Details"/>
            </div>
        </div>

    </form:form>

    <div class="row">
        <div class="small-10 columns right">
            <h6>New Users Active Invites</h6>
            <c:choose>
                <c:when test="${not empty pendingNew}">
                    <table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="400">Email</th>
                            <th width="400">Role</th>
                            <th width="260">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${pendingNew}" var="pending" begin="0" step="1" varStatus="status">
                            <tr>
                                <td>${status.count}</td>
                                <td>${pending.email}</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${pending.is_admin == 1}">
                                            Admin
                                        </c:when>
                                        <c:otherwise>
                                            Member
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <a href="#"
                                       class="button secondary leaveGroup small" style="margin-bottom:0px">Resend
                                        Invite</a>
                                    <a href="<c:url value='/authenticated/mygroups/administer/${groupCn}/action/${pending.email}/removenewpend'/>"
                                       class="button alert leaveGroup small" style="margin-bottom:0px">Remove</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>There are no new users active invites.</p>
                </c:otherwise>
            </c:choose>
        </div>

    </div>

    <div class="row">
        <div class="small-10 columns right">
            <h6>Existing Users Active Invites</h6>
            <c:choose>
                <c:when test="${not empty pendingExisting}">
                    <table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="400">Member</th>
                            <th>Role</th>
                            <th width="260">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${pendingExisting}" var="pending" begin="0" step="1" varStatus="status">
                            <tr>
                                <td>${status.count}</td>
                                <td>${pending.username}</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${pending.is_admin == 1}">
                                            Admin
                                        </c:when>
                                        <c:otherwise>
                                            Member
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <a href="#"
                                       class="button secondary leaveGroup small" style="margin-bottom:0px">Resend
                                        Invite</a>
                                    <a href="<c:url value='/authenticated/mygroups/administer/${groupCn}/action/${pending.username}/removeexistingpend'/>"
                                       class="button alert leaveGroup small" style="margin-bottom:0px">Remove</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p>There are no existing users active invites.</p>
                </c:otherwise>
            </c:choose>
        </div>

    </div>
</div>
<div class="small-6 large-7 columns">

    <div class="row">
        <div class="small-10 columns right">
            <h6>Administrators &nbsp; &nbsp; <a href="#" data-dropdown="addAdminDropDown"
                                                class="button tiny round success"
                                                style="margin-bottom:0px">Add+</a></h6>
            <c:url value='/authenticated/mygroups/administer/${groupCn}/action/addadmin' var="actionUrl"/>
            <form:form id="addAdminDropDown" cssClass="f-dropdown medium" data-dropdown-content=""
                       commandName="group" style="margin-top:20px" action="${actionUrl}" method="POST">
                <div class="small-12 large-12 columns">
                    <div class="row" style="margin-left:20px;margin-top:10px;">
                        <div class="small-2 columns">
                            <form:label path="addAdminEmail" cssClass="right inline">Email: </form:label>
                        </div>
                        <div class="small-7 columns left">
                            <form:input path="addAdminEmail" size="20"/>
                            <input type="submit" class="button success small right" value="Invite">
                        </div>
                    </div>
                </div>
            </form:form>
            <table>
                <thead>
                <tr>
                    <th>#</th>
                    <th width="400">Administrator</th>
                    <th>Remove</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${group.owners}" var="owner" begin="0" step="1" varStatus="status">
                    <tr>
                        <td>${status.count}</td>
                        <td>${owner}</td>
                        <td>
                            <a href="<c:url value='/authenticated/mygroups/administer/${groupCn}/action/${owner}/removeadmin'/>"
                               class="button alert leaveGroup small"
                               style="margin-bottom:0px">Remove</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </div>

    <div class="row">
        <div class="small-10 columns right">
            <h6>Members &nbsp; &nbsp; <a href="#" data-dropdown="addMemberDropDown" class="button tiny round success"
                                         style="margin-bottom:0px">Add+</a>
                <c:url value='/authenticated/mygroups/administer/${groupCn}/action/addmember' var="actionUrl"/>
                <form:form id="addMemberDropDown" cssClass="f-dropdown medium" data-dropdown-content=""
                           commandName="group" style="margin-top:20px" action="${actionUrl}" method="POST">
                    <div class="small-12 large-12 columns">
                        <div class="row" style="margin-left:20px;margin-top:10px;">
                            <div class="small-2 columns">
                                <form:label path="addMemberEmail" cssClass="right inline">Email: </form:label>
                            </div>
                            <div class="small-7 columns left">
                                <form:input path="addMemberEmail" size="20"/>
                                <input type="submit" class="button success small right" value="Invite">
                            </div>
                        </div>
                    </div>
                </form:form>
            </h6>
            <table>
                <thead>
                <tr>
                    <th>#</th>
                    <th width="400">Member</th>
                    <th>Remove</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${group.uniqueMembers}" var="member" begin="0" step="1" varStatus="status">
                    <tr>
                        <td>${status.count}</td>
                        <td>${member}</td>
                        <td>
                            <a href="<c:url value='/authenticated/mygroups/administer/${groupCn}/action/${member}/remove'/>"
                               class="button alert leaveGroup small" style="margin-bottom:0px">Remove</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </div>
</div>
</div>


</div>

</body>
</html>