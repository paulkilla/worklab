<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <%@ page session="false" %>
    <%@ page isELIgnored="false" %>
    <title>My Groups</title>
</head>

<body>
<ul class="breadcrumbs">
    <li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
    <li><a href="#">My Groups</a></li>
    <li class="current"><a href="#">Overview</a></li>
</ul>
<h2>My Groups</h2>
<dl class="sub-nav">
    <dd class="active"><a href="<c:url value='/authenticated/mygroups'/>">Overview</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/join'/>">Join</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/leave'/>">Leave</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/administer'/>">Administer</a></dd>
    <dd>|</dd>
    <dd><a href="<c:url value='/authenticated/mygroups/create'/>">Create...</a></dd>
</dl>
<c:if test="${not empty messages}">
    <div class="alert-box success">${messages}</div>
</c:if>
<div class="panel">
    <h6>You are part of the following groups:</h6>
    <c:choose>
        <c:when test="${not empty groups}">
            <table>
                <thead>
                <tr>
                    <th width="200">Key</th>
                    <th width="200">Name</th>
                    <th>Description</th>
                    <th width="50">Public</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${groups}" var="group">
                    <tr>
                        <td><a href="<c:url value='/authenticated/group/${group.cn}'/>">${group.cn}</a></td>
                        <td>${group.groupName}</td>
                        <td>${group.description}</td>
                        <td>
                            <c:choose>
                                <c:when test="${group.publicGroup == true}">
                                    Yes
                                </c:when>
                                <c:otherwise>
                                    No
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:when>
        <c:otherwise>
            You are not in any groups.
        </c:otherwise>
    </c:choose>
</div>

</body>
</html>