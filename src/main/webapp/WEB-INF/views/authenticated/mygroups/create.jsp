<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<%@ page session="false"%>
<%@ page isELIgnored="false"%>
<title>Create Group</title>
</head>

<body>
	<ul class="breadcrumbs">
		<li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
		<li><a href="#">My Groups</a></li>
		<li class="current"><a href="#">Create...</a></li>
	</ul>
	<h2>My Groups</h2>
	<dl class="sub-nav">
		<dd>
			<a href="<c:url value='/authenticated/mygroups'/>">Overview</a>
		</dd>
		<dd>|</dd>
        <dd><a href="<c:url value='/authenticated/mygroups/join'/>">Join</a></dd>
		<dd>|</dd>
		<dd>
			<a href="<c:url value='/authenticated/mygroups/leave'/>">Leave</a>
		</dd>
		<dd>|</dd>
		<dd>
			<a href="<c:url value='/authenticated/mygroups/administer'/>">Administer</a>
		</dd>
		<dd>|</dd>
		<dd class="active">
			<a href="<c:url value='/authenticated/mygroups/create'/>">Create...</a>
		</dd>
	</dl>

	<div class="panel">
		<c:if test="${not empty messages}">
			<div class="alert-box success">${messages}</div>
		</c:if>
		<c:url var="actionUrl" value="/authenticated/mygroups/create" />
		<form:form action="${actionUrl}" autocomplete="off" id="creategroup"
			commandName="creategroup" method="POST">
			<form:errors path="*" cssClass="alert-box alert" element="div" />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="cn" cssClass="right inline">Group Key: </form:label>
						</div>
						<div class="small-7 columns left">
							<form:input path="cn" />
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="groupName" cssClass="right inline">Group Name: </form:label>
						</div>
						<div class="small-7 columns left">
							<form:input path="groupName" />
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="description" cssClass="right inline">Description: </form:label>
						</div>
						<div class="small-7 columns left">
							<form:input path="description" />
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="publicGroup" cssClass="right inline">Public Group? </form:label>
						</div>
						<div class="small-7 columns left">
							<form:checkbox path="publicGroup" value="true" />
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-5">
					<div class="row">
						<div class="small-10 columns right">
							<input type="submit" class="right button success"
								value="Create Group" />
						</div>
					</div>
				</div>
			</div>
		</form:form>
	</div>

</body>
</html>