<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <%@ page session="false" %>
    <%@ page isELIgnored="false" %>
    <title>My WorkLab</title>
</head>

<body>
<ul class="breadcrumbs">
    <li class="current"><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
</ul>

<h2>My WorkLab</h2>

<div class="row">
    <div class="large-12">

        <div class="large-5 columns">
            <div class="panel">
                <h3>My Groups</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Key</th>
                        <th width="100%">Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${groups}" var="group">
                        <tr>
                            <td><a href="<c:url value='/authenticated/group/${group.cn}'/>">${group.cn}</a></td>
                            <td>${group.groupName}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="large-7 columns">
            <div class="panel">
                <h3>Recent Activity</h3>
                <c:forEach items="${recentActivities}" var="recentActivity">
                    <div class="panel newsfeed-panel">
                        Some items here
                    </div>
                </c:forEach>
                <div class="panel newsfeed-panel">
                    Some items here
                </div>
                <div class="panel newsfeed-panel">
                    Some items here
                </div>
                <div class="panel newsfeed-panel">
                    Some items here
                </div>
                <div class="panel newsfeed-panel">
                    Some items here
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>