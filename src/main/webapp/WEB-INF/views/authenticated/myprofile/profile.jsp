<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<%@ page session="false"%>
<%@ page isELIgnored="false"%>
<title>My Profile</title>
</head>

<body>
	<ul class="breadcrumbs">
		<li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
		<li><a href="#">My Profile</a></li>
		<li class="current"><a href="#">Overview</a></li>
	</ul>
	<h2>My Profile</h2>
	<dl class="sub-nav">
		<dd class="active">
			<a href="<c:url value='/authenticated/myprofile'/>">Overview</a>
		</dd>
		<dd>|</dd>
		<dd>
			<a href="<c:url value='/authenticated/myprofile/changepassword'/>">Change Password</a>
		</dd>
		<dd>|</dd>
		<dd>
			<a href="<c:url value='/authenticated/myprofile/updatedetails'/>">Update Details</a>
		</dd>
	</dl>

	<div class="panel">
		<div class="row">
			<div class="small-4">
				<div class="row">
					<div class="small-3 columns">
						<label class="right" for="right-label">Username:</label>
					</div>
					<div class="small-7 columns">
						<label class="left" id="right-label">${user.username}</label>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="small-4">
				<div class="row">
					<div class="small-3 columns">
						<label class="right" for="right-label">First Name:</label>
					</div>
					<div class="small-7 columns">
						<label class="left" id="right-label">${user.firstname}</label>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="small-4">
				<div class="row">
					<div class="small-3 columns">
						<label class="right" for="right-label">Last Name:</label>
					</div>
					<div class="small-7 columns">
						<label class="left" id="right-label">${user.lastname}</label>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="small-4">
				<div class="row">
					<div class="small-3 columns">
						<label class="right" for="right-label">E-mail:</label>
					</div>
					<div class="small-7 columns">
						<label class="left" id="right-label">${user.email}</label>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>