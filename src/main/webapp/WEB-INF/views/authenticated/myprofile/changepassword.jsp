<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<%@ page session="false"%>
<%@ page isELIgnored="false"%>
<title>My Profile</title>
</head>

<body>
	<ul class="breadcrumbs">
		<li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
		<li><a href="<c:url value='/authenticated/myprofile'/>">My Profile</a></li>
		<li class="current"><a href="#">Change Password</a></li>
	</ul>
	<h2>Change Password</h2>
	<dl class="sub-nav">
		<dd>
			<a href="<c:url value='/authenticated/myprofile'/>">Overview</a>
		</dd>
		<dd>|</dd>
		<dd class="active">
			<a href="<c:url value='/authenticated/myprofile/changepassword'/>">Change
				Password</a>
		</dd>
		<dd>|</dd>
		<dd>
			<a href="<c:url value='/authenticated/myprofile/updatedetails'/>">Update
				Details</a>
		</dd>
	</dl>

	<div class="panel">
		<c:if test="${not empty messages}">
			<div class="alert-box success">${messages}</div>
		</c:if>
		<c:url var="actionUrl" value="/authenticated/myprofile/changepassword" />
		<form:form action="${actionUrl}" autocomplete="off" id="changepassword"
			commandName="changepassword" method="POST">
			<form:errors path="*" cssClass="alert-box alert" element="div" />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="newPassword" cssClass="right inline">Password: </form:label>
						</div>
						<div class="small-7 columns left">
							<form:password path="newPassword" />
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="confirmPassword" cssClass="right inline">Confirm Password: </form:label>
						</div>
						<div class="small-7 columns left">
							<form:password path="confirmPassword" />
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-5">
					<div class="row">
						<div class="small-10 columns right">
							<input type="submit" class="right button success"
								value="Change Password" />
						</div>
					</div>
				</div>
			</div>
		</form:form>
	</div>

</body>
</html>