<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<%@ page session="false"%>
<%@ page isELIgnored="false"%>
<title>My Profile</title>
</head>

<body>
	<ul class="breadcrumbs">
		<li><a href="<c:url value='/authenticated'/>">My WorkLab</a></li>
		<li><a href="<c:url value='/authenticated/myprofile'/>">My Profile</a></li>
		<li class="current"><a href="#">Update Details</a></li>
	</ul>
	<h2>Update details</h2>
	<dl class="sub-nav">
		<dd>
			<a href="<c:url value='/authenticated/myprofile'/>">Overview</a>
		</dd>
		<dd>|</dd>
		<dd>
			<a href="<c:url value='/authenticated/myprofile/changepassword'/>">Change
				Password</a>
		</dd>
		<dd>|</dd>
		<dd class="active">
			<a href="<c:url value='/authenticated/myprofile/updatedetails'/>">Update
				Details</a>
		</dd>
	</dl>

	<div class="panel">
		<c:if test="${not empty messages}">
			<div class="alert-box success">${messages}</div>
		</c:if>
		<c:url var="actionUrl" value="/authenticated/myprofile/updatedetails" />
		<form:form action="${actionUrl}" autocomplete="off" id="updatedetails"
			commandName="updatedetails" method="POST">
			<form:errors path="*" cssClass="alert-box alert" element="div" />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="username" cssClass="right inline">Username: </form:label>
						</div>
						<div class="small-7 columns left">
							<form:label path="username" cssClass="inline">${username}</form:label>
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="firstname" cssClass="right inline">First Name: </form:label>
						</div>
						<div class="small-7 columns left">
							<form:input path="firstname" />
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="lastname" cssClass="right inline">Last Name: </form:label>
						</div>
						<div class="small-7 columns left">
							<form:input path="lastname" />
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-6">
					<div class="row">
						<div class="small-3 columns">
							<form:label path="email" cssClass="right inline">E-mail: </form:label>
						</div>
						<div class="small-7 columns left">
							<form:input path="email" />
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="small-5">
					<div class="row">
						<div class="small-10 columns right">
							<input type="submit" class="right button success"
								value="Update Details" />
						</div>
					</div>
				</div>
			</div>
		</form:form>
	</div>

</body>
</html>