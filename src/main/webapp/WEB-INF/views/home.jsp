<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <%@ page session="false" %>
    <%@ page isELIgnored="false" %>
    <title>Login</title>
</head>

<body>
<div class="row">
    <div class="large-4 large-centered columns">
    <h3>Login with Username and Password (Custom Page)</h3>
        <div class="large-6 large-centered columns">
            <c:if test="${not empty error}">
                <div class="errorblock">
                    Your login attempt was not successful, try again.<br/> Caused :
                        ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
                </div>
            </c:if>
            <form id="login" action="<c:url value='j_spring_security_check' />"
                  method="POST">

                <table>
                    <tr>
                        <td>Username:</td>
                        <td><input type='text' name='j_username' value=''></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type='password' name='j_password'/></td>
                    </tr>
                    <tr>
                        <td><input name="submit" type="submit" value="Login"/></td>
                        <td><a href="<c:url value='/register'/>">Register</a></td>
                    </tr>
                </table>

            </form>
        </div>

</div>
</div>

</div>
</body>
</html>