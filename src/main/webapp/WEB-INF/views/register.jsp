<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <%@ page session="true" %>
    <%@ page isELIgnored="false" %>
    <title>Register</title>
</head>

<body>
<c:if test="${not empty messages}">
    <div class="alert-box alert">${messages}</div>
</c:if>
<c:url var="actionUrl" value="/register"/>
<form:form action="${actionUrl}" autocomplete="off" id="register" commandName="registration" method="POST"
           style="padding-left:20px">
    <form:errors path="*"/>
    <form:label path="firstname">First Name: </form:label><form:input path="firstname"/><br/>
    <form:label path="lastname">Surname: </form:label><form:input path="lastname"/><br/>
    <c:choose>
        <c:when test="${registration.activationCode != null}">
            <form:label path="email">E-mail: </form:label> <form:input path="email" disabled="true"/><form:hidden
                path="email"/><form:hidden
                                path="activationCode"/><br/>
        </c:when>
        <c:otherwise>
            <form:label path="email">E-mail: </form:label> <form:input path="email"/><br/>
        </c:otherwise>
    </c:choose>

    <form:label path="newPassword">Password: </form:label><form:password path="newPassword"/><br/>
    <form:label path="confirmPassword">Confirm Password: </form:label><form:password path="confirmPassword"/><br/>

    <input type="submit" value="Register"/>
</form:form>
</body>
</html>