package au.com.pazzul.worklab.controller;

import au.com.pazzul.worklab.ldap.LdapDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Handles requests for the application home page.
 */
@Controller
public class LogoutController {

    private static final Logger logger = LoggerFactory.getLogger(LogoutController.class);

    @Autowired
    private LdapDAO ldapDao;

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        logger.info("Logging out");
        request.getSession().removeAttribute("sessionUsername");
        SecurityContextHolder.clearContext();
        Cookie cookie = new Cookie("SPRING_SECURITY_REMEMBER_ME_COOKIE", null);
        cookie.setMaxAge(0);
        cookie.setPath(request.getContextPath().length() > 0 ? request.getContextPath() : "/");
        response.addCookie(cookie);
        return "home";
    }
}
