package au.com.pazzul.worklab.controller;

import au.com.pazzul.worklab.db.DatabaseDAO;
import au.com.pazzul.worklab.jpa.PendingNew;
import au.com.pazzul.worklab.ldap.LdapDAO;
import au.com.pazzul.worklab.model.Registration;
import au.com.pazzul.worklab.validator.RegistrationValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Handles requests for the application home page.
 */
@Controller
public class RegistrationController {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    private LdapDAO ldapDao;

    @Autowired
    private DatabaseDAO databaseDao;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView register(Model model) {
        logger.info("Welcome home!");
        ModelAndView mav = new ModelAndView("register");
        mav.addObject("registration", new Registration());

        return mav;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView doRegister(@ModelAttribute("registration")
                                   Registration registration, BindingResult result) {
        ModelAndView mav;
        RegistrationValidator validator = new RegistrationValidator();
        validator.validate(registration, result);
        if (result.hasErrors()) {
            mav = new ModelAndView("register");
            mav.addObject("registration", registration);
            mav.addAllObjects(result.getModel());
        } else {
            try {
                String username = ldapDao.createUser(registration);
                if(registration.getActivationCode() != null) {
                    //This means it was an activation, lookup the e-mail address in the db and add all the groups the user was invites to.
                    List<PendingNew> newGroups = databaseDao.findPendingNewByEmailActivationCode(registration.getEmail(), registration.getActivationCode());
                    for(PendingNew pending: newGroups) {
                        if(pending.getIs_admin() == 1) {
                            ldapDao.addToGroupAsOwner(username, pending.getGroup_id().getGroup_key());
                        }

                        ldapDao.addToGroup(username, pending.getGroup_id().getGroup_key());
                    }
                    databaseDao.removePendingNewUserGroups(newGroups);
                }

                mav = new ModelAndView("home");
            } catch (Exception e) {
                //Email already exists
                mav = new ModelAndView("register");
                mav.addObject("registration", registration);
                result.rejectValue("email", "error.007");
                mav.addAllObjects(result.getModel());
            }
        }

        return mav;
    }

    @RequestMapping(value = "/register/activate", method = RequestMethod.GET)
    public ModelAndView registerActivation(@RequestParam("e")String emailQS, @RequestParam("c")String activationCodeQS, Model model, HttpServletRequest request) {
        request.getSession().removeAttribute("emailForActivation");
        logger.info("Welcome activation!");
        ModelAndView mav = new ModelAndView("register");
        Registration newRegistration = new Registration();
        String email = databaseDao.validActivationCode(emailQS, activationCodeQS);
        if(email != null) {
            newRegistration.setEmail(email);
            newRegistration.setActivationCode(activationCodeQS);
        } else {
            mav.addObject("messages", "Code not found, but you can still register and join public communities!");
        }

        mav.addObject("registration", newRegistration);

        return mav;
    }
}
