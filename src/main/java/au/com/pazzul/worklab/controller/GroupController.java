package au.com.pazzul.worklab.controller;

import au.com.pazzul.worklab.db.DatabaseDAO;
import au.com.pazzul.worklab.jpa.Pages;
import au.com.pazzul.worklab.model.LdapGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.pazzul.worklab.ldap.LdapDAO;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Handles requests for the application home page.
 */
@Controller
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    private LdapDAO ldapDao;


    @Autowired
    private DatabaseDAO databaseDAO;


    @RequestMapping(value = "/authenticated/group/{groupCn}", method = RequestMethod.GET)
    public ModelAndView groupHome(@PathVariable("groupCn") String groupCn, Model model) {
        logger.info("Welcome viewing group " + groupCn);
        LdapGroup group = ldapDao.findGroup(groupCn);
        ModelAndView mav = new ModelAndView("authenticated/group/newsfeed");
        mav.addObject("group", group);

        return mav;
    }

    @RequestMapping(value = "/authenticated/group/{groupCn}/pages", method = RequestMethod.GET)
    public ModelAndView groupPages(@PathVariable("groupCn") String groupCn, Model model) {
        logger.info("Welcome viewing pages " + groupCn);
        LdapGroup group = ldapDao.findGroup(groupCn);
        ModelAndView mav = new ModelAndView("authenticated/group/pages/pages");
        mav.addObject("group", group);
        mav.addObject("pages", databaseDAO.getPagesForGroup(groupCn));

        return mav;
    }

    @RequestMapping(value = "/authenticated/group/{groupCn}/page/{pageId}/show", method = RequestMethod.GET)
    public ModelAndView groupPage(@PathVariable("groupCn") String groupCn, @PathVariable("pageId") String pageId, Model model) {
        logger.info("Welcome viewing page " + groupCn + " " + pageId);
        LdapGroup group = ldapDao.findGroup(groupCn);
        ModelAndView mav = new ModelAndView("authenticated/group/pages/show");
        mav.addObject("group", group);
        mav.addObject("page", databaseDAO.getPage(groupCn, pageId));

        return mav;
    }

    @RequestMapping(value = "/authenticated/group/{groupCn}/posts", method = RequestMethod.GET)
    public ModelAndView groupPosts(@PathVariable("groupCn") String groupCn, Model model) {
        logger.info("Welcome viewing posts " + groupCn);
        LdapGroup group = ldapDao.findGroup(groupCn);
        ModelAndView mav = new ModelAndView("authenticated/group/posts/posts");
        mav.addObject("group", group);

        return mav;
    }

    @RequestMapping(value = "/authenticated/group/{groupCn}/attachments", method = RequestMethod.GET)
    public ModelAndView groupAttachments(@PathVariable("groupCn") String groupCn, Model model) {
        logger.info("Welcome viewing attachments " + groupCn);
        LdapGroup group = ldapDao.findGroup(groupCn);
        ModelAndView mav = new ModelAndView("authenticated/group/attachments/attachments");
        mav.addObject("group", group);

        return mav;
    }

    @RequestMapping(value = "/authenticated/group/{groupCn}/events", method = RequestMethod.GET)
    public ModelAndView groupEvents(@PathVariable("groupCn") String groupCn, Model model) {
        logger.info("Welcome viewing events " + groupCn);
        LdapGroup group = ldapDao.findGroup(groupCn);
        ModelAndView mav = new ModelAndView("authenticated/group/events/events");
        mav.addObject("group", group);

        return mav;
    }

    @RequestMapping(value = "/authenticated/group/{groupCn}/tasks", method = RequestMethod.GET)
    public ModelAndView groupTasks(@PathVariable("groupCn") String groupCn, Model model) {
        logger.info("Welcome viewing tasks " + groupCn);
        LdapGroup group = ldapDao.findGroup(groupCn);
        ModelAndView mav = new ModelAndView("authenticated/group/tasks/tasks");
        mav.addObject("group", group);

        return mav;
    }

    /* Controller for creating editing contents */

    @RequestMapping(value = "/authenticated/group/{groupCn}/pages/create", method = RequestMethod.GET)
    public ModelAndView groupPagesCreate(@PathVariable("groupCn") String groupCn, Model model) {
        logger.info("Welcome creating page " + groupCn);
        LdapGroup group = ldapDao.findGroup(groupCn);
        ModelAndView mav = new ModelAndView("authenticated/group/pages/create");
        mav.addObject("group", group);
        mav.addObject("createpage", new Pages());

        return mav;
    }

    @RequestMapping(value = "/authenticated/group/{groupCn}/pages/create", method = RequestMethod.POST)
    public ModelAndView groupPagesCreateAction(@PathVariable("groupCn") String groupCn, @ModelAttribute("createpage") Pages page, Model model, Principal principal) {
        logger.info("Welcome creating page " + page.getTitle() + " for " + groupCn);
        page.setCreator(principal.getName());
        page.setRevision_id(1);
        page.setDate_created(new Timestamp(new Date().getTime()));
        Pages savedPage = databaseDAO.savePage(groupCn, page);

        return groupPages(groupCn, model);
    }

}
