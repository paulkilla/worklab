package au.com.pazzul.worklab.controller;

import au.com.pazzul.worklab.ldap.LdapDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private LdapDAO ldapDao;

    /**
     * Simply selects the home view to render by returning its name.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        logger.info("Welcome home!");

        return "home";
    }

    @RequestMapping(value = "/authenticated", method = RequestMethod.GET)
    public String myWorkLab(ModelMap model, Principal principal, HttpServletRequest request) {
        logger.info("Inside /authenticated.");
        if (principal == null) {
            return "redirect:/";
        }
        String name = principal.getName();
        request.getSession().setAttribute("sessionUsername", name);
        model.addAttribute("username", name);
        model.addAttribute("favouriteGroups", null);
        model.addAttribute("groups", ldapDao.getAllGroupsForUser(name));
        return "authenticated/myWorkLab";

    }


}
