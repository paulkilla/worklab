package au.com.pazzul.worklab.controller;

import au.com.pazzul.worklab.db.DatabaseDAO;
import au.com.pazzul.worklab.jpa.Group;
import au.com.pazzul.worklab.ldap.LdapDAO;
import au.com.pazzul.worklab.model.LdapGroup;
import au.com.pazzul.worklab.model.LdapUser;
import au.com.pazzul.worklab.validator.CreateGroupValidator;
import au.com.pazzul.worklab.validator.UpdateGroupValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MyGroupsController {

    private static final Logger logger = LoggerFactory.getLogger(MyGroupsController.class);

    @Autowired
    private LdapDAO ldapDao;

    @Autowired
    private DatabaseDAO databaseDao;

    @RequestMapping(value = "/authenticated/mygroups", method = RequestMethod.GET)
    public ModelAndView myGroups(Model model, Principal principal) {
        logger.info("Welcome mygroups!");
        ModelAndView mav = new ModelAndView("authenticated/mygroups/groups");
        List<LdapGroup> groups = ldapDao.getAllGroupsForUser(principal.getName());
        mav.addObject("groups", groups);

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/join", method = RequestMethod.GET)
    public ModelAndView joinGroups(Model model, Principal principal) {
        logger.info("Welcome joinGroups!");
        ModelAndView mav = new ModelAndView("authenticated/mygroups/join");
        List<LdapGroup> groups = ldapDao.getAllPublicGroups();
        List<LdapGroup> newGroups = new ArrayList<LdapGroup>();
        for (LdapGroup group : groups) {
            if (!ldapDao.isUserMemberOfGroup(principal.getName(), group.getCn())) {
                newGroups.add(group);
            }
        }
        mav.addObject("groups", newGroups);

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/{groupCn}/join", method = RequestMethod.GET)
    public ModelAndView joinGroupsAction(@PathVariable("groupCn") String groupCn, Model model, Principal principal) {
        logger.info("Welcome joinGroupsAction!");

        ldapDao.addToGroup(principal.getName(), groupCn);
        ModelAndView mav = myGroups(model, principal);
        mav.setViewName("authenticated/mygroups/groups");
        mav.addObject("messages", "You have successfully joined " + groupCn);

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/leave", method = RequestMethod.GET)
    public ModelAndView leaveGroups(Model model, Principal principal) {
        logger.info("Welcome leaveGroups!");
        ModelAndView mav = new ModelAndView("authenticated/mygroups/leave");
        List<LdapGroup> groups = ldapDao.getAllPublicGroupsForUser(principal.getName());
        mav.addObject("groups", groups);

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/leave/{groupCn}", method = RequestMethod.GET)
    public ModelAndView leaveGroupAction(@PathVariable("groupCn") String groupCn, Model model, Principal principal) {
        logger.info("Welcome leaveGroupAction!");
        ldapDao.removeFromGroup(principal.getName(), groupCn);
        ModelAndView mav = myGroups(model, principal);
        mav.addObject("messages", "You have been removed from the '" + groupCn + "' group.");
        mav.setViewName("authenticated/mygroups/groups");

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/administer", method = RequestMethod.GET)
    public ModelAndView administerGroups(Model model, Principal principal) {
        logger.info("Welcome administerGroups!");
        ModelAndView mav = new ModelAndView("authenticated/mygroups/administer");
        List<LdapGroup> groups = ldapDao.getAllOwnerGroupsForUser(principal.getName());
        mav.addObject("groups", groups);

        return mav;
    }


    @RequestMapping(value = "/authenticated/mygroups/create", method = RequestMethod.GET)
    public String createGroup(ModelMap model, Principal principal) {
        model.addAttribute("creategroup", new LdapGroup());
        return "authenticated/mygroups/create";
    }

    @RequestMapping(value = "/authenticated/mygroups/create", method = RequestMethod.POST)
    public ModelAndView createGroupAction(@ModelAttribute("creategroup") LdapGroup group, Model oldModel, ModelMap model, Principal principal, BindingResult result) {
        ModelAndView mav = new ModelAndView();
        CreateGroupValidator validator = new CreateGroupValidator();
        validator.validate(group, result);

        if (result.hasErrors()) {
            mav.addAllObjects(result.getModel());
            mav.setViewName("authenticated/mygroups/create");
        } else {
            try {
                ldapDao.createGroup(principal.getName(), group);
                Group dbGroup = new Group();
                dbGroup.setGroup_key(group.getCn());
                databaseDao.saveGroup(dbGroup);
                ModelAndView successMav = myGroups(oldModel, principal);
                successMav.addObject("messages", "Group '" + group.getGroupName() + "' successfully created.");
                return successMav;
            } catch (Exception e) {
                e.printStackTrace();
                result.rejectValue("cn", "error.008");
                mav.addAllObjects(result.getModel());
                mav.setViewName("authenticated/mygroups/create");
            }
        }

        return mav;
    }


    @RequestMapping(value = "/authenticated/mygroups/administer/{groupCn}", method = RequestMethod.GET)
    public ModelAndView administerGroup(@PathVariable("groupCn") String groupCn, Model model, String messages) {
        logger.info("Welcome administering group " + groupCn);
        ModelAndView mav = new ModelAndView("authenticated/mygroups/administer/administer");
        LdapGroup group = ldapDao.findGroup(groupCn);
        mav.addObject("group", group);
        mav.addObject("groupCn", groupCn);

        mav.addObject("pendingNew", databaseDao.findPendingNewByGroupKey(groupCn));
        mav.addObject("pendingExisting", databaseDao.findPendingExistingByGroupKey(groupCn));

        if (messages != null) {
            mav.addObject("messages", messages);
        }

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/administer/{groupCn}", method = RequestMethod.POST)
    public ModelAndView administerGroupAction(@PathVariable("groupCn") String groupCn, @ModelAttribute("group") LdapGroup group, ModelMap model, Principal principal, BindingResult result) {
        logger.info("Welcome updating group " + groupCn);
        ModelAndView mav = new ModelAndView("authenticated/mygroups/administer/administer");
        mav.addObject("groupCn", groupCn);
        UpdateGroupValidator validator = new UpdateGroupValidator();
        validator.validate(group, result);
        if (result.hasErrors()) {
            mav.addAllObjects(result.getModel());
        } else {
            LdapGroup newGroup = new LdapGroup();
            newGroup.setCn(groupCn);
            newGroup.setGroupName(group.getGroupName());
            newGroup.setDescription(group.getDescription());
            newGroup.setPublicGroup(group.isPublicGroup());
            ldapDao.updateGroupDetails(newGroup);
            mav.setViewName("authenticated/mygroups/administer/administer");
            mav.addObject("messages", "Group details successfully updated.");
        }
        mav.addObject("group", ldapDao.findGroup(groupCn));

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/administer/{groupCn}/action/{uid}/remove", method = RequestMethod.GET)
    public ModelAndView administerGroupAction(@PathVariable("groupCn") String groupCn, @PathVariable("uid") String uid, ModelMap modelMap, Model model, Principal principal) {
        logger.info("Welcome updating group " + groupCn + " removing " + uid);
        ldapDao.removeFromGroup(uid, groupCn);
        ModelAndView mav = administerGroup(groupCn, model, "User " + uid + " removed successfully.");

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/administer/{groupCn}/action/{uid}/removeadmin", method = RequestMethod.GET)
    public ModelAndView administerGroupRemoveAdminAction(@PathVariable("groupCn") String groupCn, @PathVariable("uid") String uid, ModelMap modelMap, Model model, Principal principal) {
        logger.info("Welcome updating group " + groupCn + " removing " + uid);
        ldapDao.removeFromAdmin(uid, groupCn);
        ModelAndView mav = administerGroup(groupCn, model, "User " + uid + " administration rights removed successfully.");

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/administer/{groupCn}/action/{email}/removenewpend", method = RequestMethod.GET)
    public ModelAndView administerGroupRemoveNewUserPendingAction(@PathVariable("groupCn") String groupCn, @PathVariable("email") String email, ModelMap modelMap, Model model, Principal principal) {
        logger.info("Welcome updating group " + groupCn + " removing " + email);
        databaseDao.removePendingNewUser(email, groupCn);
        ModelAndView mav = administerGroup(groupCn, model, "User " + email + " removed successfully.");

        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/administer/{groupCn}/action/{username}/removeexistingpend", method = RequestMethod.GET)
    public ModelAndView administerGroupRemoveExistingUserPendingAction(@PathVariable("groupCn") String groupCn, @PathVariable("username") String username, ModelMap modelMap, Model model, Principal principal) {
        logger.info("Welcome updating group " + groupCn + " removing " + username);
        databaseDao.removePendingExistingUser(username, groupCn);
        ModelAndView mav = administerGroup(groupCn, model, "User " + username + " removed successfully.");
        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/administer/{groupCn}/action/addadmin", method = RequestMethod.POST)
    public ModelAndView administerGroupAddAdmin(@PathVariable("groupCn") String groupCn, @ModelAttribute("group") LdapGroup group, ModelMap modelMap, Model model, Principal principal) {
        logger.info("Welcome adding new member to group " + groupCn + " email " + group.getAddAdminEmail());

        LdapUser user = ldapDao.findUserByEmail(group.getAddAdminEmail());
        String message;
        if (user != null) {
            ldapDao.addToGroupAsOwner(user.getUsername(), groupCn);
            message = "Existing user " + user.getUsername() + " with e-mail address " + group.getAddAdminEmail() + " added successfully.";
        } else {
            databaseDao.addPendingNewUser(group.getAddAdminEmail(), groupCn, new Integer(1));
            message = "User " + group.getAddAdminEmail() + " invites to join " + groupCn + ".";
        }

        ModelAndView mav = administerGroup(groupCn, model, message);
        return mav;
    }

    @RequestMapping(value = "/authenticated/mygroups/administer/{groupCn}/action/addmember", method = RequestMethod.POST)
    public ModelAndView administerGroupAddMember(@PathVariable("groupCn") String groupCn, @ModelAttribute("group") LdapGroup group, ModelMap modelMap, Model model, Principal principal) {
        logger.info("Welcome adding new member to group " + groupCn + " email " + group.getAddMemberEmail());

        LdapUser user = ldapDao.findUserByEmail(group.getAddMemberEmail());
        String message;
        if (user != null) {
            ldapDao.addToGroup(user.getUsername(), groupCn);
            message = "Existing user " + user.getUsername() + " with e-mail address " + group.getAddMemberEmail() + " added successfully.";
        } else {
            databaseDao.addPendingNewUser(group.getAddMemberEmail(), groupCn, new Integer(0));
            message = "User " + group.getAddMemberEmail() + " invites to join " + groupCn + ".";
        }

        ModelAndView mav = administerGroup(groupCn, model, message);
        return mav;
    }


}
