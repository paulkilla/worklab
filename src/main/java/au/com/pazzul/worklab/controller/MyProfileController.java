package au.com.pazzul.worklab.controller;

import au.com.pazzul.worklab.ldap.LdapDAO;
import au.com.pazzul.worklab.model.LdapUser;
import au.com.pazzul.worklab.model.Registration;
import au.com.pazzul.worklab.validator.ChangePasswordValidator;
import au.com.pazzul.worklab.validator.UpdateDetailsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MyProfileController {

    private static final Logger logger = LoggerFactory.getLogger(MyProfileController.class);

    @Autowired
    private LdapDAO ldapDao;

    @RequestMapping(value = "/authenticated/myprofile", method = RequestMethod.GET)
    public String myProfile(ModelMap model, Principal principal) {
        LdapUser user = ldapDao.findUser(principal.getName());

        model.addAttribute("user", user);
        return "authenticated/myprofile/profile";
    }

    @RequestMapping(value = "/authenticated/myprofile/changepassword", method = RequestMethod.GET)
    public ModelAndView changePassword(ModelMap model, Principal principal) {
        ModelAndView mav = new ModelAndView("authenticated/myprofile/changepassword");
        mav.addObject("changepassword", new Registration());
        return mav;
    }

    @RequestMapping(value = "/authenticated/myprofile/changepassword", method = RequestMethod.POST)
    public ModelAndView changePasswordAction(@ModelAttribute("changepassword") Registration user, ModelMap model, Principal principal, BindingResult result) {
        ModelAndView mav = new ModelAndView("authenticated/myprofile/changepassword");
        ChangePasswordValidator validator = new ChangePasswordValidator();
        validator.validate(user, result);
        if (result.hasErrors()) {
            mav.addAllObjects(result.getModel());
        } else {
            ldapDao.updatePassword(principal.getName(), user.getNewPassword());
            mav.addObject("messages", "Password successfully updated.");
        }

        return mav;
    }

    @RequestMapping(value = "/authenticated/myprofile/updatedetails", method = RequestMethod.GET)
    public String updateDetails(ModelMap model, Principal principal) {
        LdapUser user = ldapDao.findUser(principal.getName());
        model.addAttribute("updatedetails", user);
        model.addAttribute("username", principal.getName());
        return "authenticated/myprofile/updatedetails";
    }

    @RequestMapping(value = "/authenticated/myprofile/updatedetails", method = RequestMethod.POST)
    public ModelAndView updateDetailsAction(@ModelAttribute("updatedetails") Registration user, ModelMap model, Principal principal, BindingResult result) {
        ModelAndView mav = new ModelAndView("authenticated/myprofile/updatedetails");
        mav.addObject("username", principal.getName());
        UpdateDetailsValidator validator = new UpdateDetailsValidator();
        validator.validate(user, result);
        if (result.hasErrors()) {
            mav.addAllObjects(result.getModel());
        } else {
            LdapUser newUser = new LdapUser();
            newUser.setFirstname(user.getFirstname());
            newUser.setLastname(user.getLastname());
            newUser.setEmail(user.getEmail());
            ldapDao.updateUserDetails(principal.getName(), newUser);
            mav.addObject("messages", "User details successfully updated.");
        }

        return mav;
    }


}
