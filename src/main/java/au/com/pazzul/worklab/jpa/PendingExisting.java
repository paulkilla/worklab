package au.com.pazzul.worklab.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: HOME
 * Date: 8/13/13
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "pending_existing")
public class PendingExisting implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer pending_exist_id = null;
    private Integer is_admin = null;
    private Group group_id = null;
    private String username = null;
    private Timestamp date_invited = null;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getPending_exist_id() {
        return pending_exist_id;
    }

    public void setPending_exist_id(Integer pending_exist_id) {
        this.pending_exist_id = pending_exist_id;
    }

    public Integer getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(Integer is_admin) {
        this.is_admin = is_admin;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUP_ID", nullable = false)
    public Group getGroup_id() {
        return group_id;
    }

    public void setGroup_id(Group group_id) {
        this.group_id = group_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Timestamp getDate_invited() {
        return date_invited;
    }

    public void setDate_invited(Timestamp date_invited) {
        this.date_invited = date_invited;
    }
}
