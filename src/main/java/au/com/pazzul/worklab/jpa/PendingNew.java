package au.com.pazzul.worklab.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: HOME
 * Date: 8/13/13
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="pending_new")
public class PendingNew implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer pending_new_id = null;
    private Integer is_admin = null;
    private Group group_id = null;
    private String email = null;
    private String activation_code = null;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getPending_new_id() {
        return pending_new_id;
    }

    public void setPending_new_id(Integer pending_new_id) {
        this.pending_new_id = pending_new_id;
    }

    public Integer getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(Integer is_admin) {
        this.is_admin = is_admin;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GROUP_ID", nullable = false)
    public Group getGroup_id() {
        return group_id;
    }

    public void setGroup_id(Group group_id) {
        this.group_id = group_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActivation_code() {
        return activation_code;
    }

    public void setActivation_code(String activation_code) {
        this.activation_code = activation_code;
    }
}
