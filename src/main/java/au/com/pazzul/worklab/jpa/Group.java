package au.com.pazzul.worklab.jpa;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: HOME
 * Date: 8/13/13
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="\"group\"")
public class Group implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer group_id = null;
    private String group_key = null;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "GROUP_ID")
    public Integer getGroup_id() {
        return group_id;
    }

    public void setGroup_id(Integer group_id) {
        this.group_id = group_id;
    }

    public String getGroup_key() {
        return group_key;
    }

    public void setGroup_key(String group_key) {
        this.group_key = group_key;
    }

}
