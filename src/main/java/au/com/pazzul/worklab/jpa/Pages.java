package au.com.pazzul.worklab.jpa;

import au.com.pazzul.worklab.helper.TimeHelper;
import au.com.pazzul.worklab.model.HTMLContent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: HOME
 * Date: 8/13/13
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "pages")
public class Pages implements Serializable {

    private static final long serialVersionUID = 1L;
    private PagePrimaryKey id;

    //  private Integer page_id = null;
    private Integer revision_id = null;
    //  private Group group_id = null;
    private String creator = null;
    private String title = null;
    private HTMLContent content = null;
    private Timestamp date_created = null;

    private String prettyTime = null;

   /* @Id
    public Integer getPage_id() {
        return page_id;
    }

    public void setPage_id(Integer page_id) {
        this.page_id = page_id;
    }

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GROUP_ID")
    public Group getGroup_id() {
        return group_id;
    }

    public void setGroup_id(Group group_id) {
        this.group_id = group_id;
    }                 */

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "groupId", column = @Column(name = "GROUP_ID", nullable = false, length = 1)),
            @AttributeOverride(name = "pageId", column = @Column(name = "PAGE_ID", nullable = false))})
    public PagePrimaryKey getId() {
        return id;
    }

    public void setId(PagePrimaryKey id) {
        this.id = id;
    }

    public Integer getRevision_id() {
        return revision_id;
    }

    public void setRevision_id(Integer revision_id) {
        this.revision_id = revision_id;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Lob
    public HTMLContent getContent() {
        return content;
    }

    public void setContent(HTMLContent content) {
        this.content = content;
    }

    public Timestamp getDate_created() {
        return date_created;
    }

    public void setDate_created(Timestamp date_created) {
        this.date_created = date_created;
        TimeHelper timeHelper = new TimeHelper();
        String timeUntil = timeHelper.timeAgo(getDate_created().getTime());
        this.prettyTime = timeUntil;
    }

    @Transient
    public String getPrettyTime() {
        TimeHelper timeHelper = new TimeHelper();
        String timeUntil = timeHelper.timeAgo(getDate_created().getTime());
        return timeUntil;
    }
}
