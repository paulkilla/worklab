package au.com.pazzul.worklab.jpa;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: HOME
 * Date: 8/22/13
 * Time: 7:41 PM
 * To change this template use File | Settings | File Templates.
 */
@Embeddable
public class PagePrimaryKey implements Serializable {

    private static final long serialVersionUID = 1L;

    private int groupId;
    private int pageId;

    public PagePrimaryKey() {
    }

    public PagePrimaryKey(int groupId, int pageId) {
        this.groupId = groupId;
        this.pageId = pageId;
    }

    @Column(name = "GROUP_ID", nullable = false, length = 1)
    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Column(name = "PAGE_ID", nullable = false)
    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public boolean equals(Object other) {
    		if ((this == other))
    			return true;
    		if ((other == null))
    			return false;
    		if (!(other instanceof PagePrimaryKey))
    			return false;
    		PagePrimaryKey castOther = (PagePrimaryKey) other;

    		return (this.getGroupId() == castOther.getGroupId())
    				&& (this.getPageId() == castOther.getPageId());
    	}

    	public int hashCode() {
    		int result = 17;

    		result = 37 * result + this.getGroupId();
    		result = 37 * result + this.getPageId();
    		return result;
    	}
}
