package au.com.pazzul.worklab.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.pazzul.worklab.model.Registration;

public class UpdateDetailsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		if(clazz.equals(Registration.class)) {
			return true;
		}
		return false;
	}

	@Override
	public void validate(Object object, Errors errors) {
		Registration registration = (Registration)object;
		String firstname = registration.getFirstname();
		String lastname = registration.getLastname();
		String email = registration.getEmail();
		
		if(firstname.trim().equals("")) {
			errors.rejectValue("firstname", "error.001");
		}
		if(lastname.trim().equals("")) {
			errors.rejectValue("lastname", "error.002");
		}
		if(email.trim().equals("")) {
			errors.rejectValue("email", "error.003");
		}
		
	}

}
