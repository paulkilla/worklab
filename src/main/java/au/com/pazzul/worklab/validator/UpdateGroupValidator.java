package au.com.pazzul.worklab.validator;

import au.com.pazzul.worklab.model.LdapGroup;
import au.com.pazzul.worklab.model.Registration;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UpdateGroupValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		if(clazz.equals(LdapGroup.class)) {
			return true;
		}
		return false;
	}

	@Override
	public void validate(Object object, Errors errors) {
        LdapGroup group = (LdapGroup)object;
		String groupName = group.getGroupName();
		String groupDescription = group.getDescription();
		
		if(groupName.trim().equals("")) {
			errors.rejectValue("groupName", "error.010");
		}
		if(groupDescription.trim().equals("")) {
			errors.rejectValue("description", "error.011");
		}
	}

}
