package au.com.pazzul.worklab.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.pazzul.worklab.model.Registration;

public class ChangePasswordValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		if(clazz.equals(Registration.class)) {
			return true;
		}
		return false;
	}

	@Override
	public void validate(Object object, Errors errors) {
		Registration registration = (Registration)object;
		
		String password = registration.getNewPassword();
		String confirmPassword = registration.getConfirmPassword();
		
		if(password.trim().equals("")) {
			errors.rejectValue("newPassword", "error.004");
		}
		if(confirmPassword.trim().equals("")) {
			errors.rejectValue("confirmPassword", "error.005");
		}
		
		if(!confirmPassword.trim().equals("") && !password.trim().equals("") && !password.equals(confirmPassword)) {
			errors.rejectValue("newPassword", "error.006");
		}
	}

}
