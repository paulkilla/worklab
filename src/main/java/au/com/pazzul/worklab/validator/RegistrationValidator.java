package au.com.pazzul.worklab.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.pazzul.worklab.model.Registration;

public class RegistrationValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		if(clazz.equals(Registration.class)) {
			return true;
		}
		return false;
	}

	@Override
	public void validate(Object object, Errors errors) {
		Registration registration = (Registration)object;
		String firstname = registration.getFirstname();
		String lastname = registration.getLastname();
		String email = registration.getEmail();
		String password = registration.getNewPassword();
		String confirmPassword = registration.getConfirmPassword();
		
		if(firstname.trim().equals("")) {
			errors.rejectValue("firstname", "error.001");
		}
		if(lastname.trim().equals("")) {
			errors.rejectValue("lastname", "error.002");
		}
		if(email.trim().equals("")) {
			errors.rejectValue("email", "error.003");
		}
		if(password.trim().equals("")) {
			errors.rejectValue("password", "error.004");
		}
		if(confirmPassword.trim().equals("")) {
			errors.rejectValue("confirmPassword", "error.005");
		}
		
		if(!confirmPassword.trim().equals("") && !password.trim().equals("") && !password.equals(confirmPassword)) {
			errors.rejectValue("password", "error.006");
		}
	}

}
