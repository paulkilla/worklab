package au.com.pazzul.worklab.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.pazzul.worklab.model.LdapGroup;
import au.com.pazzul.worklab.model.Registration;

public class CreateGroupValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		if(clazz.equals(LdapGroup.class)) {
			return true;
		}
		return false;
	}

	@Override
	public void validate(Object object, Errors errors) {
		LdapGroup registration = (LdapGroup)object;
		String cn = registration.getCn();
		String name = registration.getDescription();
		String description = registration.getGroupName();
		
		if(cn.trim().equals("")) {
			errors.rejectValue("cn", "error.009");
		}
		if(name.trim().equals("")) {
			errors.rejectValue("groupName", "error.010");
		}
		if(description.trim().equals("")) {
			errors.rejectValue("description", "error.011");
		}
	}

}
