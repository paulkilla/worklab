package au.com.pazzul.worklab.helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: HOME
 * Date: 8/12/13
 * Time: 7:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorkLabHelper {

    public static String usernameFromDn(String uid) {
        if (uid.contains("=") && uid.contains(",")) {
            return uid.substring(uid.indexOf("=") + 1, uid.indexOf(","));
        } else {
            return uid;
        }
    }

    public static String[] usernamesFromDn(String[] uids) {
        List<String> usernames = new ArrayList<String>(uids.length);
        for (String uid : uids) {
            usernames.add(usernameFromDn(uid));
        }

        return usernames.toArray(new String[usernames.size()]);
    }

}
