package au.com.pazzul.worklab.db;


import au.com.pazzul.worklab.jpa.Group;
import au.com.pazzul.worklab.jpa.Pages;
import au.com.pazzul.worklab.jpa.PendingExisting;
import au.com.pazzul.worklab.jpa.PendingNew;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

public interface DatabaseDAO {

    public Collection<Group> findGroups(final int startIndex, final int maxResults);

    public Collection<Group> findGroups();

    public Collection<Group> findGroupByGroupKey(String groupKey);

    public Group saveGroup(Group group);

    public PendingNew savePendingNew(PendingNew pendingNew);

    public PendingExisting savePendingExisting(PendingExisting pendingExisting);

    public Collection<PendingNew> findPendingNewByGroupKey(String groupKey);

    public Collection<PendingExisting> findPendingExistingByGroupKey(String groupKey);

    public void removePendingNewUser(String email, String groupKey);

    public void removePendingExistingUser(String username, String groupKey);

    public PendingNew addPendingNewUser(String email, String groupKey, Integer isAdmin);

    public boolean activationCodeExistsPendingNew(String activationCode);

    public String validActivationCode(String email, String activationCode);

    public List<PendingNew> findPendingNewByEmailActivationCode(String email, String activationCode);

    public List<PendingNew> getPendingNewUserGroups(String email);

    public void removePendingNewUserGroups(List<PendingNew> pendingNew);

    public Pages savePage(String groupKey, Pages page);

    public List<Pages> getPagesForGroup(String groupKey);

    public Pages getPage(String groupKey, String pageId);
}
