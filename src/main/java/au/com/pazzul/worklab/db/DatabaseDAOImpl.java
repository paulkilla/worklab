package au.com.pazzul.worklab.db;

import au.com.pazzul.worklab.jpa.*;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

@Service
@Repository
@Transactional(readOnly = true)
public class DatabaseDAOImpl implements DatabaseDAO {

    private EntityManager em = null;

    /**
     * Sets the entity manager.
     */
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    /**
     * Find persons using a start index and max number of results.
     */
    @SuppressWarnings("unchecked")
    public Collection<Group> findGroups(final int startIndex, final int maxResults) {
        return em.createQuery("select g from Group g order by g.group_key, g.group_id")
                .setFirstResult(startIndex).setMaxResults(maxResults).getResultList();
    }

    /**
     * Find persons.
     */
    @SuppressWarnings("unchecked")
    public Collection<Group> findGroups() {
        return em.createQuery("select g from Group g order by g.group_key, g.group_id").getResultList();
    }

    /**
     * Find persons by last name.
     */
    @SuppressWarnings("unchecked")
    public Collection<Group> findGroupByGroupKey(String groupKey) {
        return em.createQuery("select g from Group g where g.group_key = :groupKey order by g.group_key, g.group_id")
                .setParameter("groupKey", groupKey).getResultList();
    }

    /**
     * Saves person.
     */
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Group saveGroup(Group group) {
        return em.merge(group);
    }


    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public PendingNew savePendingNew(PendingNew pendingNew) {
        return em.merge(pendingNew);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public PendingExisting savePendingExisting(PendingExisting pendingExisting) {
        return em.merge(pendingExisting);
    }

    @Override
    public Collection<PendingNew> findPendingNewByGroupKey(String groupKey) {
        Collection<Group> group = findGroupByGroupKey(groupKey);
        return em.createQuery("select p from PendingNew p where p.group_id = :groupId order by p.email")
                .setParameter("groupId", group).getResultList();
    }

    @Override
    public Collection<PendingExisting> findPendingExistingByGroupKey(String groupKey) {
        Collection<Group> group = findGroupByGroupKey(groupKey);
        return em.createQuery("select p from PendingExisting p where p.group_id = :groupId order by p.username")
                .setParameter("groupId", group).getResultList();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void removePendingNewUser(String email, String groupKey) {
        Collection<Group> group = findGroupByGroupKey(groupKey);
        em.remove(em.createQuery("select p from PendingNew p where p.group_id = :groupId and p.email = :email").setParameter("groupId", group).setParameter("email", email).getSingleResult());
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void removePendingExistingUser(String username, String groupKey) {
        Collection<Group> group = findGroupByGroupKey(groupKey);
        em.remove(em.createQuery("select p from PendingExisting p where p.group_id = :groupId and p.username = :username").setParameter("groupId", group).setParameter("username", username).getSingleResult());
    }


    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public PendingNew addPendingNewUser(String email, String groupKey, Integer isAdmin) {
        String randomCode = RandomStringUtils.randomAlphanumeric(10);
        while (activationCodeExistsPendingNew(randomCode)) {
            randomCode = RandomStringUtils.randomAlphabetic(10);
        }

        Collection<Group> group = findGroupByGroupKey(groupKey);
        PendingNew pendingNew = new PendingNew();
        pendingNew.setEmail(email);
        pendingNew.setGroup_id(group.iterator().next());
        pendingNew.setActivation_code(randomCode);
        pendingNew.setIs_admin(isAdmin);
        return em.merge(pendingNew);
    }

    @Override
    public boolean activationCodeExistsPendingNew(String activationCode) {
        List<PendingNew> pendingNew = em.createQuery("select p from PendingNew p where p.activation_code = :activationCode").setParameter("activationCode", activationCode).getResultList();
        if (!pendingNew.isEmpty()) {
            return true;
        }
        return false;
    }

    @Override
    public String validActivationCode(String email, String activationCode) {
        PendingNew pendingNew = new PendingNew();
        pendingNew.setEmail(email);
        pendingNew.setActivation_code(activationCode);
        List<PendingNew> newPendings = em.createQuery("select p from PendingNew p where p.activation_code = :activationCode and p.email = :email").setParameter("activationCode", activationCode).setParameter("email", email).getResultList();

        if (!newPendings.isEmpty()) {
            return email;
        } else {
            return null;
        }
    }

    @Override
    public List<PendingNew> findPendingNewByEmailActivationCode(String email, String activationCode) {
        if (this.validActivationCode(email, activationCode) != null) {
            return this.getPendingNewUserGroups(email);
        } else {
            return null;
        }
    }

    @Override
    public List<PendingNew> getPendingNewUserGroups(String email) {
        return em.createQuery("select p from PendingNew p where p.email = :email").setParameter("email", email).getResultList();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void removePendingNewUserGroups(List<PendingNew> pendingNew) {
        for (PendingNew pending : pendingNew) {
            PendingNew remove = em.merge(pending);
            em.remove(remove);
        }
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Pages savePage(String groupKey, Pages page) {

        Collection<Group> group = findGroupByGroupKey(groupKey);
        Group theGroup = group.iterator().next();
        Long nextPageForGroup = findNextPageForGroup(theGroup);
        int nextPage = nextPageForGroup.intValue();
        page.setId(new PagePrimaryKey(theGroup.getGroup_id(), nextPage + 1));

        return em.merge(page);
    }

    @Override
    public List<Pages> getPagesForGroup(String groupKey) {
        Collection<Group> group = findGroupByGroupKey(groupKey);
        return em.createQuery("select p from Pages p where p.id.groupId = :group_id order by p.date_created desc").setParameter("group_id", group.iterator().next().getGroup_id()).getResultList();
    }

    @Transactional
    private Long findNextPageForGroup(Group group) {
        return (Long)em.createQuery("select count(p) from Pages p where p.id.groupId = :group_id").setParameter("group_id", group.getGroup_id()).getResultList().get(0);
    }

    @Override
    public Pages getPage(String groupKey, String pageId) {
        Collection<Group> group = findGroupByGroupKey(groupKey);
        return (Pages)em.createQuery("select p from Pages p where p.id.groupId = :group_id and p.id.pageId = :page_id").setParameter("group_id", group.iterator().next().getGroup_id()).setParameter("page_id", Integer.valueOf(pageId)).getSingleResult();
    }

}
