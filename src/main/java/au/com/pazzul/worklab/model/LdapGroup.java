package au.com.pazzul.worklab.model;

import java.util.List;

public class LdapGroup {
	
	private String cn;
	private String groupName;
	private String description;
	private boolean publicGroup;
	private String[] uniqueMembers;
    private String[] owners;

    private String addAdminEmail;
    private String addMemberEmail;
	
	
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String[] getUniqueMembers() {
		return uniqueMembers;
	}
	public void setUniqueMembers(String[] uniqueMembers) {
		this.uniqueMembers = uniqueMembers;
	}
	public boolean isPublicGroup() {
		return publicGroup;
	}
	public void setPublicGroup(boolean publicGroup) {
		this.publicGroup = publicGroup;
	}
    public String[] getOwners() {
        return owners;
    }
    public void setOwners(String[] owners) {
        this.owners = owners;
    }


    public String getAddAdminEmail() {
        return addAdminEmail;
    }

    public void setAddAdminEmail(String addAdminEmail) {
        this.addAdminEmail = addAdminEmail;
    }

    public String getAddMemberEmail() {
        return addMemberEmail;
    }

    public void setAddMemberEmail(String addMemberEmail) {
        this.addMemberEmail = addMemberEmail;
    }
}
