package au.com.pazzul.worklab.model;

import java.util.List;

public class LdapUser {
	
	private String username;
	private String firstname;
	private String lastname;
	private String email;
	private String givenName;
	private List<LdapGroup> groups;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getFullname() {
		return firstname + " " + lastname;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public List<LdapGroup> getGroups() {
		return groups;
	}
	public void setGroups(List<LdapGroup> groups) {
		this.groups = groups;
	}
	
	

}
