package au.com.pazzul.worklab.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: HOME
 * Date: 8/20/13
 * Time: 8:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class HTMLContent implements Serializable {

    private static final long serialVersionUID = 1L;

    private String htmlContent;

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }
}
