package au.com.pazzul.worklab.ldap;

import java.util.List;

import au.com.pazzul.worklab.model.LdapGroup;
import au.com.pazzul.worklab.model.LdapUser;
import au.com.pazzul.worklab.model.Registration;

public interface LdapDAO {
	
	public String createUser(Registration registration) throws Exception;
	public boolean userExists(String username);
	public List<LdapGroup> getAllGroupsForUser(String username);
	public List<LdapGroup> getAllPublicGroupsForUser(String username);
	public List<LdapGroup> getAllPublicGroups();
	public List<LdapGroup> getAllOwnerGroupsForUser(String username);
	public Boolean isUserMemberOfGroup(String username, String cn);
	
	public LdapUser findUser(String username);
    public LdapUser findUserByEmail(String email);
	public void updatePassword(String username, String password);
	public void updateUserDetails(String username, LdapUser user);
	
    public LdapGroup findGroup(String cn);
    public void createGroup(String username, LdapGroup group) throws Exception;
	public boolean groupExists(String username);
	public void removeFromGroup(String username, String cn);
    public void removeFromAdmin(String username, String cn);
    public void updateGroupDetails(LdapGroup group);
    public void addToGroup(String username, String cn);
    public void addToGroupAsOwner(String username, String cn);

}
