package au.com.pazzul.worklab.ldap;

import java.util.List;

import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;

import au.com.pazzul.worklab.helper.WorkLabHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.NameNotFoundException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.WhitespaceWildcardsFilter;
import org.springframework.stereotype.Service;

import au.com.pazzul.worklab.model.LdapGroup;
import au.com.pazzul.worklab.model.LdapUser;
import au.com.pazzul.worklab.model.Registration;

@Service
public class LdapDAOImpl implements LdapDAO {

    @Autowired
    private LdapTemplate ldapTemplate;

    @Override
    public String createUser(Registration registration) throws Exception {
        int count = 1;
        String username = registration.getFirstname().toLowerCase() + "."
                            + registration.getLastname().toLowerCase();
        try {
            // Do check on email.
            if (emailExists(registration.getEmail())) {
                throw new Exception("Email already exists");
            }

            // Make sure username is unique
            while (userExists(username)) {
                username = registration.getFirstname().toLowerCase() + "."
                        + registration.getLastname().toLowerCase() + count;
                count++;
            }
            registration.setUsername(username);

            // Create user
            ldapTemplate.bind(buildDnForUser(username), null,
                    buildRegistrationAttributes(registration));

            // Add user to group worklab (default group required to login)
            Name dn = buildDnForGroup("worklab");
            Attribute attr = new BasicAttribute("uniqueMember", "uid="
                    + username + ",ou=users,dc=pazzul,dc=com,dc=au");
            ModificationItem item = new ModificationItem(
                    DirContext.ADD_ATTRIBUTE, attr);
            ldapTemplate.modifyAttributes(dn, new ModificationItem[]{item});

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return username;
    }

    @Override
    public boolean userExists(String username) {
        boolean exists = true;
        try {
            ldapTemplate.lookup(buildDnForUser(username));
        } catch (NameNotFoundException nfe) {
            exists = false;
        }
        return exists;
    }

    @Override
    public boolean groupExists(String cn) {
        boolean exists = true;
        try {
            ldapTemplate.lookup(buildDnForGroup(cn));
        } catch (NameNotFoundException nfe) {
            exists = false;
        }
        return exists;
    }

    @Override
    public List<LdapGroup> getAllGroupsForUser(String username) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "groupOfUniqueNames")).and(
                new WhitespaceWildcardsFilter("uniqueMember", "uid=" + username
                        + ",ou=users,dc=pazzul,dc=com,dc=au"));
        DistinguishedName dn = new DistinguishedName();
        dn.add("ou", "groups");
        List<LdapGroup> userGroups = ldapTemplate.search(dn, filter.encode(),
                new GroupContextMapper());
        return userGroups;
    }

    @Override
    public List<LdapGroup> getAllPublicGroupsForUser(String username) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "groupOfUniqueNames"))
                .and(new WhitespaceWildcardsFilter("businessCategory", "public"))
                .and(new WhitespaceWildcardsFilter("uniqueMember", "uid="
                        + username + ",ou=users,dc=pazzul,dc=com,dc=au"));
        DistinguishedName dn = new DistinguishedName();
        dn.add("ou", "groups");
        List<LdapGroup> userGroups = ldapTemplate.search(dn, filter.encode(),
                new GroupContextMapper());
        return userGroups;
    }

    @Override
    public List<LdapGroup> getAllPublicGroups() {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "groupOfUniqueNames")).and(
                new WhitespaceWildcardsFilter("businessCategory", "public"));
        DistinguishedName dn = new DistinguishedName();
        dn.add("ou", "groups");
        List<LdapGroup> userGroups = ldapTemplate.search(dn, filter.encode(),
                new GroupContextMapper());
        return userGroups;
    }

    @Override
    public List<LdapGroup> getAllOwnerGroupsForUser(String username) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "groupOfUniqueNames")).and(
                new WhitespaceWildcardsFilter("owner", "uid=" + username
                        + ",ou=users,dc=pazzul,dc=com,dc=au"));
        DistinguishedName dn = new DistinguishedName();
        dn.add("ou", "groups");
        List<LdapGroup> userGroups = ldapTemplate.search(dn, filter.encode(),
                new GroupContextMapper());
        return userGroups;
    }

    @Override
    public Boolean isUserMemberOfGroup(String username, String cn) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "groupOfUniqueNames")).and(
                new WhitespaceWildcardsFilter("uniqueMember", "uid=" + username
                        + ",ou=users,dc=pazzul,dc=com,dc=au"));

        List<LdapGroup> userGroups = ldapTemplate.search(buildDnForGroup(cn),
                filter.encode(), new GroupContextMapper());
        if (userGroups != null && userGroups.size() > 0) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public LdapUser findUser(String username) {
        LdapUser user = (LdapUser) ldapTemplate.lookup(
                buildDnForUser(username), new UserContextMapper());
        return user;
    }

    @Override
    public LdapUser findUserByEmail(String email) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "person")).and(
                new WhitespaceWildcardsFilter("mail", email));
        DistinguishedName dn = new DistinguishedName();
        dn.add("ou", "users");
        List<LdapUser> users = ldapTemplate.search(dn, filter.encode(),
                new UserContextMapper());

        if (!users.isEmpty()) {
            return users.get(0);
        } else {
            return null;
        }
    }

    @Override
    public void updatePassword(String username, String password) {
        // Add user to group worklab (default group required to login)
        Name dn = buildDnForUser(username);
        Attribute attr = new BasicAttribute("userPassword", password);
        ModificationItem item = new ModificationItem(
                DirContext.REPLACE_ATTRIBUTE, attr);
        ldapTemplate.modifyAttributes(dn, new ModificationItem[]{item});
    }

    @Override
    public void updateUserDetails(String username, LdapUser user) {
        Name dn = buildDnForUser(username);

        Attribute givenNameAttr = new BasicAttribute("givenName",
                user.getFirstname());
        ModificationItem givenNameItem = new ModificationItem(
                DirContext.REPLACE_ATTRIBUTE, givenNameAttr);
        Attribute snAttr = new BasicAttribute("sn", user.getLastname());
        ModificationItem snItem = new ModificationItem(
                DirContext.REPLACE_ATTRIBUTE, snAttr);
        Attribute mailAttr = new BasicAttribute("mail", user.getEmail());
        ModificationItem mailItem = new ModificationItem(
                DirContext.REPLACE_ATTRIBUTE, mailAttr);
        ldapTemplate.modifyAttributes(dn, new ModificationItem[]{
                givenNameItem, snItem, mailItem});
    }

    @Override
    public void createGroup(String username, LdapGroup group) throws Exception {

        try {
            // Do check on email.
            if (groupExists(group.getCn())) {
                throw new Exception("Group with that key already exists");
            }


            // Create group
            ldapTemplate.bind(buildDnForGroup(group.getCn()), null,
                    buildGroupAttributes(username, group));
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void removeFromGroup(String username, String cn) {
        Name dn = buildDnForGroup(cn);

        Attribute uniqueMemberAttr = new BasicAttribute("uniqueMember",
                "uid=" + username + ",ou=users,dc=pazzul,dc=com,dc=au");
        ModificationItem uniqueMemberItem = new ModificationItem(
                DirContext.REMOVE_ATTRIBUTE, uniqueMemberAttr);
        ldapTemplate.modifyAttributes(dn, new ModificationItem[]{
                uniqueMemberItem});
    }

    @Override
    public void removeFromAdmin(String username, String cn) {
        Name dn = buildDnForGroup(cn);

        Attribute uniqueMemberAttr = new BasicAttribute("owner",
                "uid=" + username + ",ou=users,dc=pazzul,dc=com,dc=au");
        ModificationItem uniqueMemberItem = new ModificationItem(
                DirContext.REMOVE_ATTRIBUTE, uniqueMemberAttr);
        ldapTemplate.modifyAttributes(dn, new ModificationItem[]{
                uniqueMemberItem});
    }

    @Override
    public LdapGroup findGroup(String cn) {
        LdapGroup group = (LdapGroup) ldapTemplate.lookup(
                buildDnForGroup(cn), new GroupContextMapper());
        return group;
    }

    @Override
    public void updateGroupDetails(LdapGroup group) {
        Name dn = buildDnForGroup(group.getCn());
        String publicPrivate = "private";
        if (group.isPublicGroup()) {
            publicPrivate = "public";
        }
        Attribute groupNameAttr = new BasicAttribute("o",
                group.getGroupName());
        ModificationItem groupNameItem = new ModificationItem(
                DirContext.REPLACE_ATTRIBUTE, groupNameAttr);
        Attribute descriptionAttr = new BasicAttribute("description", group.getDescription());
        ModificationItem descriptionItem = new ModificationItem(
                DirContext.REPLACE_ATTRIBUTE, descriptionAttr);
        Attribute publicGroupAttr = new BasicAttribute("businessCategory", publicPrivate);
        ModificationItem publicGroupItem = new ModificationItem(
                DirContext.REPLACE_ATTRIBUTE, publicGroupAttr);
        ldapTemplate.modifyAttributes(dn, new ModificationItem[]{
                groupNameItem, descriptionItem, publicGroupItem});
    }

    @Override
    public void addToGroup(String username, String cn) {
        Name dn = buildDnForGroup(cn);
        Attribute uniqueMemberAttr = new BasicAttribute("uniqueMember",
                "uid=" + username + ",ou=users,dc=pazzul,dc=com,dc=au");
        ModificationItem uniqueMemberItem = new ModificationItem(
                DirContext.ADD_ATTRIBUTE, uniqueMemberAttr);
        ldapTemplate.modifyAttributes(dn, new ModificationItem[]{
                uniqueMemberItem});
    }

    @Override
    public void addToGroupAsOwner(String username, String cn) {
        Name dn = buildDnForGroup(cn);
        Attribute uniqueMemberAttr = new BasicAttribute("owner",
                "uid=" + username + ",ou=users,dc=pazzul,dc=com,dc=au");
        ModificationItem uniqueMemberItem = new ModificationItem(
                DirContext.ADD_ATTRIBUTE, uniqueMemberAttr);
        ldapTemplate.modifyAttributes(dn, new ModificationItem[]{
                uniqueMemberItem});
    }


    private Attributes buildRegistrationAttributes(Registration r) {
        Attributes attrs = new BasicAttributes();
        BasicAttribute ocattr = new BasicAttribute("objectclass");
        ocattr.add("inetOrgPerson");
        ocattr.add("organizationalPerson");
        ocattr.add("uidObject");
        ocattr.add("top");
        ocattr.add("person");
        attrs.put(ocattr);
        attrs.put("cn", r.getUsername());
        attrs.put("sn", r.getLastname());
        attrs.put("givenName", r.getFirstname());
        attrs.put("mail", r.getEmail());
        attrs.put("userPassword", r.getNewPassword());

        return attrs;
    }

    private Attributes buildGroupAttributes(String owner, LdapGroup group) {
        String publicPrivate = "private";
        if (group.isPublicGroup()) {
            publicPrivate = "public";
        }
        Attributes attrs = new BasicAttributes();
        BasicAttribute ocattr = new BasicAttribute("objectclass");
        ocattr.add("top");
        ocattr.add("groupOfUniqueNames");
        attrs.put(ocattr);
        attrs.put("cn", group.getCn());
        attrs.put("businessCategory", publicPrivate);
        attrs.put("description", group.getDescription());
        attrs.put("o", group.getGroupName());
        attrs.put("uniqueMember", "uid=" + owner + ",ou=users,dc=pazzul,dc=com,dc=au");
        attrs.put("owner", "uid=" + owner + ",ou=users,dc=pazzul,dc=com,dc=au");

        return attrs;
    }

    protected Name buildDnForUser(String uid) {
        DistinguishedName dn = new DistinguishedName();
        dn.add("ou", "users");
        dn.add("uid", uid);
        return dn;
    }

    protected Name buildDnForGroup(String cn) {
        DistinguishedName dn = new DistinguishedName();
        dn.add("ou", "groups");
        dn.add("cn", cn);
        return dn;
    }

    protected boolean emailExists(String email) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "person"));
        filter.and(new EqualsFilter("mail", email));
        List list = ldapTemplate.search("", filter.encode(),
                new AttributesMapper() {
                    public Object mapFromAttributes(Attributes attrs)
                            throws NamingException {
                        return attrs.get("mail").get();
                    }
                });
        if (list != null && list.size() > 0) {
            return true;
        } else {
            return false;
        }

    }

    private static class GroupContextMapper implements ContextMapper {
        public LdapGroup mapFromContext(Object ctx) {
            DirContextAdapter context = (DirContextAdapter) ctx;
            LdapGroup g = new LdapGroup();
            g.setCn(context.getStringAttribute("cn"));
            g.setGroupName(context.getStringAttribute("o"));
            g.setDescription(context.getStringAttribute("description"));
            if (context.getStringAttributes("owner") != null) {
                g.setOwners(WorkLabHelper.usernamesFromDn(context.getStringAttributes("owner")));
            }
            if (context.getStringAttributes("uniqueMember") != null) {
                g.setUniqueMembers(WorkLabHelper.usernamesFromDn(context.getStringAttributes("uniqueMember")));
            }
            boolean publicGroup = false;
            if (context.getStringAttribute("businessCategory").equals("public")) {
                publicGroup = true;
            }
            g.setPublicGroup(publicGroup);
            return g;
        }
    }

    private static class UserContextMapper implements ContextMapper {
        public LdapUser mapFromContext(Object ctx) {
            DirContextAdapter context = (DirContextAdapter) ctx;
            LdapUser u = new LdapUser();
            u.setUsername(context.getStringAttribute("uid"));
            u.setFirstname(context.getStringAttribute("givenName"));
            u.setLastname(context.getStringAttribute("sn"));
            u.setEmail(context.getStringAttribute("mail"));
            return u;
        }
    }
}
